console.log("BomKySu_ChromeExtension is running..");

window.addEventListener("load", () => {
    console.log("window.addEventListener load");
    setTimeout(callWith500msDelay, 500);
}); // only work after reload

function callWith500msDelay() {
    console.log("callWith500msDelay has been called..");
    updateTitle();
}

function updateTitle(){
    console.log("updateTitle has been called..");
    re = /\w+-\d+/g
    elTitle = document.querySelector("title");
    strTicket = elTitle.innerText.match(re)[0];
    strTicket_w = strTicket.match(/\w+/);
    strTicket_d = strTicket.match(/\d+/);
    strTicketNew = strTicket_d + '-' + strTicket_w;
    elTitle.innerText = strTicketNew + ' | ' + elTitle.innerText;
}
